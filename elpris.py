#!/bin/python3

# Niels Elgaard Larsen

import json
from pprint import pprint
import re
import string
import sys
import os
import datetime
import urllib.request
import urllib.error
import pprint

#pp = pprint.PrettyPrinter(indent=2)

startd=datetime.datetime.now()
start=startd.strftime("%Y-%m-%dT%H:00")
prisurl="https://api.energidataservice.dk/dataset/Elspotprices?offset=0&start="+start+"&end="+(datetime.date.today()+datetime.timedelta(days=2)).isoformat()+"T00:00&filter=%7B%22PriceArea%22:[%22DK2%22]%7D&sort=HourUTC%20ASC&timezone=dk"
#print("url",prisurl)
jpriser = urllib.request.urlopen(prisurl)
priser=json.loads(jpriser.read().decode('utf8'))

#pp.pprint(priser)
print(
"""<html>
   <head>
     <title>Elpriser &Oslash;st</title>
     <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
     <meta http-equiv="Pragma" content="no-cache" />
     <meta http-equiv="Expires" content="0" />
     <meta http-equiv="refresh" content="1000">
     <script>
      t0=new Date();
      document.addEventListener("resume", onResume, false);
      function onResume() {
           if (new Date() - t0 >  1200000) {
           location.reload();
         }
       }
     </script>
     </head>
    <body>
    <style>
      td {text-align: right;vertical-align:top;}
      th {background: #c6e386;}
      caption {background: #e19dd1;}
   </style>
   <table><tr><td>
   <table>
"""
)

day=-1
for pris in priser["records"]:
    t=datetime.datetime.fromisoformat(pris["HourDK"])
    if t.day != day or t.hour==8 or t.hour==16:
      if day>0:
          print("</table></td><td><table>")
      day=t.day
      print("<caption>",t.strftime("%a"),"</caption>")
    print("<tr><th>",t.strftime("%H"),"</th>","<td>",round(int(pris["SpotPriceDKK"])/10),"</td></tr>")
print("</table></td><tr></table>")
print(startd.strftime("%Y-%m-%d %H:%M"))
print("</body></html>")
