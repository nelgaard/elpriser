# elpriser

## Show prices of electricity, without taxes for the next day or two.

Generates very simple static HTML so that it can be used on old devices, e.g., old smartphones, tablets etc.

## requirements

* python3
* a webserver (any webserver that can serve static files will do)

## deploying


you can e.g. run the script from a /etc/cron.hourly that pipes the output to a HTML file, that can be server by you webserver.
